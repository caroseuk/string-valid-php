<?php
/*
A string is considered valid if all characters of the string appear the same number of times. 
It is also valid if you can remove just 1 character at 1 index in the string, and the remaining characters will occur the same number of times. 

Given a string , determine if it is valid. If so, return YES, otherwise return NO.

Test Cases:
aabbcd
NO 

aabbccddeefghi
NO

aaaabbcc
NO

abcdefghhgfedecba
YES

a
NO
*/

// Define function
function isValid($s) {

    // Show string to user
    echo $s."\n";

    // Initialise an array for storing the letters
    $letters = [];

    // Loop through all letters of the alphabet
    foreach (range('a', 'z') as $letter) {
        
        // If the letter exists add it
        if (substr_count($s, $letter)) {
            
            // DEBUG
            //echo substr_count($str, $letter);
            $letters[] = substr_count($s, $letter);
        }
    }
    // DEBUG
    //print_r($letters);

    // Set a variable $max with the returned highest value in the $letters array
    $max = max($letters);

    // DEBUG
    //echo "Max: ".$max."\n";

    // Set a variable $min with the returned lowest value in the $letters array
    $min = min($letters);

    // DEBUG
    //echo "Min: ".$min."\n";

    // If $max and $min are the same values, then 
    if ($max == $min) {
        // Check if only 1 character was entered, if 1 or less then NO (Ternary Logic)
        echo (strlen($letter) <= 1 ? "NO\n" : "YES\n");
    } 
    // array_count_values($letters) returns an array using the values of $letters as keys and their frequency in array as values
    else if ((array_count_values($letters)[$min] == 1 && ($min == 1 || $min - $max == 1)) || (array_count_values($letters)[$max] == 1 && ($max == 1 || $max - $min == 1))) 
    {
        // Return Yes
        echo "YES\n";
    } else {
        // Return No
        echo "NO\n";
    }

}


// Call the function against the provided strings
isValid("aabbcd");
isValid("aabbccddeefghi");
isValid("aaaabbcc");
isValid("abcdefghhgfedecba");
isValid("a");
?>