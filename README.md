# Code Challenge

Is String Valid - Written in PHP

### Running the script
To execute, run the following command

```
user@workstation$> php string-valid.php
```
### Output
You should receive an output similar to:

```
aabbcd
NO
aabbccddeefghi
NO
aaaabbcc
NO
abcdefghhgfedecba
YES
a
NO
```

DEBUG comments are also available and can be uncommented.